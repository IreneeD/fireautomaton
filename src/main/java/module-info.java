module online.airnez.fireautomaton {
    requires javafx.controls;
    requires javafx.fxml;
    requires atlantafx.base;
    requires com.fasterxml.jackson.databind;


    opens online.airnez.fireautomaton to javafx.fxml, com.fasterxml.jackson.databind;
    exports online.airnez.fireautomaton;
}