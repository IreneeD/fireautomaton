package online.airnez.fireautomaton;

import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.ListChangeListener;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.ImageView;
import javafx.scene.image.WritableImage;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

import java.util.List;

public class FireAutomatonView {

    private final Stage stage;
    private final ForestFire forestFire;
    private final int numRows;
    private final int numCols;
    private final double propagationProbability;
    private final double simInterval;
    private final WritableImage forestImage;

    /**
     * Gathers UI description and logic. There is no controller class, considering the few user interactions available
     */
    FireAutomatonView(Stage s, SimConfig simConfig, ForestFire f){
        stage = s;
        forestFire = f;
        numRows = simConfig.numRows();
        numCols = simConfig.numCols();
        propagationProbability = simConfig.propagationProbability();
        simInterval = simConfig.simInterval();
        forestImage = new WritableImage(numCols, numRows);

        // Updates forest image pixels by updating new flame pixels and turing old ones to ashes
        forestFire.getFlames().addListener(new ListChangeListener<int[]>() {
            @Override
            public void onChanged(Change<? extends int[]> change) {
                while (change.next()) {
                    List<? extends int[]> removed = change.getRemoved();
                    List<? extends int[]> added = change.getAddedSubList();
                    for(int[] ashes : removed){
                        forestImage.getPixelWriter().setColor(ashes[1], ashes[0], Color.DARKSLATEGREY);
                    }
                    for(int[] flame : added){
                        forestImage.getPixelWriter().setColor(flame[1], flame[0], Color.RED);
                    }
                }
            }
        });
        fillGreen(); // Start with a healthy forest displayed
        setLayout();
    }

    /**
     * Separates view layout definition from view constructor for readability.
     * Still quite ugly, but quick to set up :)
     */
    private void setLayout(){
        BorderPane root = new BorderPane();

        // LEFT BAR : CONTROL AND PROPERTIES
        VBox leftControlBar = new VBox();
        VBox buttonHolder = new VBox();

        leftControlBar.setSpacing(5);
        leftControlBar.setStyle("-fx-background-color: -color-bg-inset");
        leftControlBar.setPadding(new Insets(15, 15, 15, 15));
        leftControlBar.setAlignment(Pos.TOP_CENTER);
        VBox.setMargin(buttonHolder, new Insets(10, 0, 0, 0));
        buttonHolder.setAlignment(Pos.TOP_CENTER);

        Label title = new Label("FireAutomaton");
        title.getStyleClass().add("text-bold");
        Label numRowsLabel = new Label("Rows : " + numRows);
        Label numColsLabel = new Label("Cols : " + numCols);
        Label probabilityLabel = new Label("P : " + propagationProbability);
        Label simIntervalLabel = new Label("interval (ms) : " + simInterval);

        Button startFireButton = new Button("Start fire 🔥");
        startFireButton.getStyleClass().add("danger");
        startFireButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent actionEvent) {
                //Prevents to fill itself green and start simulation if the sim is already running.
                // This exposes a design flow but less is more on this one, no time to refactor :)
                if(!forestFire.isRunning()){
                    fillGreen();
                    forestFire.runSimulation();
                }
            }
        });

        forestFire.isRunningProperty().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> observableValue, Boolean aBoolean, Boolean newBoolean) {
                if(newBoolean){
                    startFireButton.setStyle("-fx-background-color: Grey");
                } else {
                    startFireButton.setStyle("-fx-background-color: -color-button-bg");
                }
            }
        });

        leftControlBar.getChildren().add(title);
        leftControlBar.getChildren().add(numRowsLabel);
        leftControlBar.getChildren().add(numColsLabel);
        leftControlBar.getChildren().add(probabilityLabel);
        leftControlBar.getChildren().add(simIntervalLabel);
        buttonHolder.getChildren().add(startFireButton);
        leftControlBar.getChildren().add(buttonHolder);
        root.setLeft(leftControlBar);

        // CENTER ZONE : SIMULATION
        VBox center = new VBox();
        center.setAlignment(Pos.CENTER);
        center.setSpacing(10);
        ImageView forestView = new ImageView(forestImage);
        forestView.setFitHeight(680);
        forestView.setFitWidth(1000);
        forestView.setSmooth(false);
        forestView.setPreserveRatio(true);

        Label stepLabel = new Label("Step : " + forestFire.getStep());
        forestFire.stepProperty().addListener(new ChangeListener<Number>() {
            @Override
            public void changed(ObservableValue<? extends Number> observableValue, Number number, Number newNumber) {
                Platform.runLater(() -> {
                    stepLabel.setText("Step : " + newNumber);
                });
            }
        });

        center.getChildren().add(forestView);
        center.getChildren().add(stepLabel);

        root.setCenter(center);
        Scene scene = new Scene(root);
        stage.setTitle("FireAutomaton");
        stage.setScene(scene);
        stage.setMinWidth(1250);
        stage.setMinHeight(750);
    }

    private void fillGreen(){
        for (int i=0; i<numRows; i++){
            for(int j=0; j<numCols; j++){
                forestImage.getPixelWriter().setColor(j, i, Color.SEAGREEN);
            }
        }
    }

    public void show(){
        stage.show();
    }
}
