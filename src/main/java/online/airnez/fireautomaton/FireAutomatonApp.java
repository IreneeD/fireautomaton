package online.airnez.fireautomaton;

import atlantafx.base.theme.CupertinoLight;

import javafx.application.Application;
import javafx.stage.Stage;

import java.io.File;
import java.io.IOException;
import java.util.Objects;

import com.fasterxml.jackson.databind.ObjectMapper;

public class FireAutomatonApp extends javafx.application.Application {

    @Override
    public void start(Stage stage) throws IOException {
        Application.setUserAgentStylesheet(new CupertinoLight().getUserAgentStylesheet());
        String simulationConfigPath = Objects.requireNonNull(
                Thread.currentThread().getContextClassLoader().getResource("sim_parameters.json")).getPath();
        SimConfig simConfig = loadConfig(simulationConfigPath);
        ForestFire forestFire = new ForestFire(simConfig);
        stage.setOnCloseRequest(event -> {forestFire.endSimulation();}); //stop simulation on closing
        FireAutomatonView forestFireView = new FireAutomatonView(stage, simConfig, forestFire);
        forestFireView.show();
    }

    public static void main(String[] args) {
        launch(args);
    }

    /**
     * @return A SimConfig record based on config json
     */
    public SimConfig loadConfig(String configFilePath) throws IOException {
        File configFile = new File(configFilePath);
        return new ObjectMapper().readValue(configFile, SimConfig.class);
    }
}