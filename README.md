# FireAutomaton

A Forest Fire cellular automaton. GUI based on JavaFX.

![FireAutomaton_screenCapture.png](FireAutomaton_screenCapture.png)

## How to configue
Parameters are set in the *sim_parameters.json* config file :
- numRows : integer, vertical axis dimension
- numCols : integer, horizontal axis dimension
- propagationProbability : double between 0 and 1. Sets fire propagation probability.
- startFlames : List of integer arrays. Start flames coordinates. Example: "[[40, 150], [180,300], [175, 200]]"
- simInterval : integer, delay between simulation steps in milliseconds