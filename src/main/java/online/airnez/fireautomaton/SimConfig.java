package online.airnez.fireautomaton;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

/**
 * Meant to be deserialized from the json property file
 *
 * @param simInterval Fixed delay between simulation steps in milliseconds
 */
public record SimConfig(int numRows, int numCols, double propagationProbability, List<int[]> startFlames,
                        int simInterval) {
    @JsonCreator
    public SimConfig(@JsonProperty("numRows") int numRows,
                     @JsonProperty("numCols") int numCols,
                     @JsonProperty("propagationProbability") double propagationProbability,
                     @JsonProperty("startFlames") List<int[]> startFlames,
                     @JsonProperty("simInterval") int simInterval) {
        this.numRows = numRows;
        this.numCols = numCols;
        this.propagationProbability = propagationProbability;
        this.startFlames = startFlames;
        this.simInterval = simInterval;
    }
}
