package online.airnez.fireautomaton;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/**
 * Forest fire cellular automaton model. Holds the state representation and propagation logic.
 */
public class ForestFire {
    private final int numRows;
    private final int numCols;
    private final double propagationProbability;
    private final List<int[]> startFlames;
    private final int simInterval; // Fixed delay between simulation steps in milliseconds
    treeState[][] forest;
    private final ObservableList<int[]> flames; // Positions of current flames. Observed by UI to follow the model state
    private SimpleIntegerProperty step;
    private SimpleBooleanProperty isRunning;
    private ScheduledExecutorService scheduler;

    public ForestFire(SimConfig simConfig) {
        numRows = simConfig.numRows();
        numCols = simConfig.numCols();
        propagationProbability = simConfig.propagationProbability();
        startFlames = simConfig.startFlames();
        simInterval = simConfig.simInterval();
        flames = FXCollections.observableList(new ArrayList<>());
        step = new SimpleIntegerProperty(0);
        isRunning = new SimpleBooleanProperty(false);
    }

    /**
     * Sets/Resets the trees states and flame list to initial conditions for new simulation runs
     */
    private void init(){
        forest = new treeState[numRows][numCols];
        scheduler = Executors.newSingleThreadScheduledExecutor();
        step.set(0);

        for(int i = 0; i < numRows ; i++)
            for(int j=0; j < numCols; j++) {
                this.forest[i][j] = treeState.healthy;
            }
        flames.addAll(startFlames);
        for(int[] flame : startFlames){
            this.forest[flame[0]][flame[1]] = treeState.ashes;
        }
    }

    /**
     * Propagates fire one step further. Checks each flame neighbors and propagates fire according to given probability
     */
    public void propagate() {
        // Used to store new flames while iterating in flames list
        ArrayList<int[]> newFlames = new ArrayList<>();
        for (int[] flame : flames){
            int[][] neighbors = {
                    {flame[0]-1, flame[1]},
                    {flame[0]+1, flame[1]},
                    {flame[0], flame[1]-1},
                    {flame[0], flame[1]+1}
            };
            for(int[] neighbor : neighbors){
                boolean inBounds = neighbor[0] >= 0 && neighbor[0] < numRows && neighbor[1] >= 0 && neighbor[1] < numCols;
                if(inBounds){
                    if(this.forest[neighbor[0]][neighbor[1]] == treeState.healthy){
                        if(Math.random()<propagationProbability) {
                            newFlames.add(new int[]{neighbor[0], neighbor[1]});
                            // A Flame is already considered ashes as it can't burn anymore
                            forest[neighbor[0]][neighbor[1]] = treeState.ashes;
                        }
                    }
                }
            }
        }
        flames.clear();
        flames.addAll(newFlames);
    }

    /**
     * Wrapper for the propagate method including step counting and simulation end-conditions logic
     */
    public void runOneSimulationStep(){
        step.set(step.get()+1);
        this.propagate();
        if(flames.isEmpty()){ // Empty flame list here means that fire has extinguished
            scheduler.shutdown();
            isRunning.set(false);
        }
    }

    /**
     * Used to prevent user from restarting simulation while running
     */
    public boolean isRunning(){
        return isRunning.get();
    }

    /**
     * Launches scheduling logic to run simulation at desired time interval
     */
    public void runSimulation(){
        if(!isRunning()) {
            init();
            isRunning.set(true);
            scheduler.scheduleWithFixedDelay(this::runOneSimulationStep, 0, simInterval, TimeUnit.MILLISECONDS);
        }
    }

    public ObservableList<int[]> getFlames() {
        return flames;
    }

    public int getStep() {
        return step.get();
    }

    public SimpleBooleanProperty isRunningProperty() {
        return isRunning;
    }

    public SimpleIntegerProperty stepProperty() {
        return step;
    }

    /**
     * Used on stage close request to prevent the sim to continue running after closing
     */
    public void endSimulation(){
        if(isRunning()){
            scheduler.shutdown();
            isRunning.set(false);
        }
    }
}
